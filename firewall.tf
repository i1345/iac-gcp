resource "google_compute_firewall" "pub-rule-ssh" {
  name          = "${var.vpc}-pub-allow-ssh"
  network       = google_compute_network.vpc.name
  project       = var.project
  description   = "Allow access 'ssh' in network public."
  direction     = "INGRESS"
  disabled      = "false"
  source_ranges = ["0.0.0.0/0"]

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
}

resource "google_compute_firewall" "pub-rule-http" {
  name          = "${var.vpc}-pub-allow-http"
  network       = google_compute_network.vpc.name
  project       = var.project
  description   = "Allow access 'http' in network public."
  direction     = "INGRESS"
  disabled      = "false"
  source_ranges = ["0.0.0.0/0"]

  allow {
    protocol = "tcp"
    ports    = ["80", "443", "3000", "7000-7100", "8000", "8080", "9090", "9100"]
  }
}
