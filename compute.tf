/*
resource "google_compute_instance" "seven" {
  name              = "${var.vpc}-seven"
  machine_type      = "e2-micro"
  zone              = "${var.zone}"
  project           = "${var.project}"

  tags = ["dev", "pub", "seven"]

  boot_disk {
    initialize_params {
      image = "centos-8-v20211105"
    }
  }

  network_interface {
    network             = "${var.vpc}"
    subnetwork          = google_compute_subnetwork.pub.name
    subnetwork_project  = "${var.project}"

    access_config {
      // Ephemeral public IP
    }
  }

  metadata = {
    person    = "seven"
    env       = "dev"
    version   = "1"
    ssh-keys  = "${var.pub_ssh_user}:${file(var.pub_ssh_key_file)}"
  }

  metadata_startup_script = "echo whoami > whoami.txt"
}

# ----------------------------------------------------------------------

resource "google_compute_instance" "thirteenn" {
  name              = "${var.vpc}-thirteenn"
  machine_type      = "e2-micro"
  zone              = "${var.zone}"
  project           = "${var.project}"

  tags = ["dev", "priv", "thirteenn"]

  boot_disk {
    initialize_params {
      image = "ubuntu-2004-focal-v20211202"
    }
  }

  network_interface {
    network             = "${var.vpc}"
    subnetwork          = google_compute_subnetwork.priv.name
    subnetwork_project  = "${var.project}"
  }

  metadata = {
    person    = "thirteenn"
    env       = "dev"
    version   = "1"
    ssh-keys  = "${var.priv_ssh_user}:${file(var.priv_ssh_key_file)}"
  }

  metadata_startup_script = "echo whoami > whoami.txt"
}

# ----------------------------------------------------------------------

resource "google_compute_instance" "dai-bo" {
  name              = "${var.vpc}-dai-bo"
  machine_type      = "e2-standard-4"
  zone              = "${var.zone}"
  project           = "${var.project}"

  tags = ["dev", "pub", "dai-bo"]

  boot_disk {
    initialize_params {
      image = "centos-8-v20211105"
    }
  }

  network_interface {
    network             = "${var.vpc}"
    subnetwork          = google_compute_subnetwork.pub.name
    subnetwork_project  = "${var.project}"

    access_config {
      // Ephemeral public IP
    }
  }

  metadata = {
    person    = "dai-bo"
    env       = "dev"
    version   = "1"
    ssh-keys  = "${var.pub_ssh_user}:${file(var.pub_ssh_key_file)}"
  }

  metadata_startup_script = "echo whoami > whoami.txt"
}

*/