# Manages a VPC network or legacy network resource on GCP.
# https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_network
#
resource "google_compute_network" "vpc" {
  name                              = var.vpc
  project                           = var.project
  description                       = "Main VPC for the entire infrastructure of the company."
  routing_mode                      = "REGIONAL"
  auto_create_subnetworks           = false
}
