# GKE cluster
resource "google_container_cluster" "cluster" {
  name      = var.cluster_name
  location  = var.region
  project   = "${var.project}"
  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count       = 1
  description              = var.cluster_description

  network    = google_compute_network.vpc.name
  subnetwork = google_compute_subnetwork.gke.name

  addons_config {
    http_load_balancing {
      disabled = true
    }

    horizontal_pod_autoscaling {
      disabled = true
    }

    network_policy_config {
      disabled = true
    }
  }

  ip_allocation_policy {
    cluster_secondary_range_name  = google_compute_subnetwork.gke.secondary_ip_range[0].range_name
    services_secondary_range_name = google_compute_subnetwork.gke.secondary_ip_range[1].range_name
  }
}

# Separately Managed Node Pool
resource "google_container_node_pool" "node_pool" {
  project   = "${var.project}"
  # use for_each instead of count
  for_each = length(var.node_pools) != 0 ? var.node_pools : var.default_node_pools
  name     = each.key
  location = replace(
    replace(var.regional_cluster, "true", var.region),
    "false",
    var.zone,
  )

  cluster    = google_container_cluster.cluster.name
  initial_node_count = lookup(each.value, "initial_node_count", var.default_node_pools["default"]["initial_node_count"])

  # manual changes to the value in the UI make terraform want to re-create the node pool
  lifecycle {
    ignore_changes = [
      initial_node_count
    ]
  }

  version = (lookup(each.value, "node_auto_upgrade", var.default_node_pools["default"]["node_auto_upgrade"]) == "true") ? null : (lookup(each.value, "kubernetes_version", var.kubernetes_version))

  autoscaling {
    min_node_count = 1
    max_node_count = lookup(each.value, "max_node_count", var.default_node_pools["default"]["max_node_count"])
  }

  management {
    auto_repair  = lookup(each.value, "node_auto_repair", var.default_node_pools["default"]["node_auto_repair"])
    auto_upgrade = lookup(each.value, "node_auto_upgrade", var.default_node_pools["default"]["node_auto_upgrade"])
  }

  node_config {
    disk_size_gb = lookup(each.value, "node_disk_size_gb", var.default_node_pools["default"]["node_disk_size_gb"])
    disk_type    = lookup(each.value, "node_disk_type", var.default_node_pools["default"]["node_disk_type"])

    preemptible  = lookup(each.value, "preemptible", var.default_node_pools["default"]["preemptible"])
    machine_type = lookup(each.value, "machine_type", var.default_node_pools["default"]["machine_type"])

    oauth_scopes = var.oauth_scopes

    labels = {
      environment = var.environment
      name        = var.cluster_name
      pool        = each.key
      preemptible = lookup(each.value, "preemptible", var.default_node_pools["default"]["preemptible"])
      type        = lookup(each.value, "type", null)
      zone        = var.zone != "" ? var.zone : null
    }

    tags = [
      var.cluster_name,
      var.environment,
      each.key,
    ]

    metadata = {
      disable-legacy-endpoints = "true"
    }

    dynamic "workload_metadata_config" {
      for_each = range(lookup(each.value, "enable_workload_identity", var.enable_workload_identity) ? 1 : 0)
      content {
        mode = "GKE_METADATA"
      }
    }

    dynamic "taint" {
      for_each = compact(split(",", lookup(each.value, "taints", "")))
      content {
        key    = split("=", taint.value)[0]
        value  = split("=", split(":", taint.value)[0])[1]
        effect = split(":", taint.value)[1]
      }
    }
  }
}
