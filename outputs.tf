output "gateway_address" {
  value       = google_compute_subnetwork.gke.*.gateway_address
  description = "The IP address of the gateway."
}

output "subnetwork_self_link" {
  value       = google_compute_subnetwork.gke.self_link
  description = "The URL of the created subnetwork"
}

# --------------------------------------------------------------
# Compute Output
#
/*
output "google_compute_instance_ipv4_internal-dai-bo" {
  value       = google_compute_instance.dai-bo.network_interface.0.network_ip
  description = "The internal ip address of the instance, either manually or dynamically assigned."
}

output "google_compute_instance_ipv4_external-dai-bo" {
  value       = google_compute_instance.dai-bo.network_interface.0.access_config.0.nat_ip 
  description = "The internal ip address of the instance, either manually or dynamically assigned."
}
*/