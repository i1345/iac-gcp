variable "project" {
  type        = string
  description = "The project name"
}

variable "vpc" {
  type        = string
  description = "The target network"
}

variable "environment" {
  type        = string
  description = "The environment name"
}

variable "region" {
  type        = string
  description = "The target region"
}

variable "zone" {
  type        = string
  description = "The target zone for compute instances"
}

# -------------------------------------------------
# Sub Network Config
#
variable "subnet_nat_name" {
  type        = string
  description = "Name of the network subnet and nat object that is created. Defaults to gke-{var.name}-{var.environment}"
  default     = null
}

variable "ip_cidr_range" {
  type          = string
  description   = "The IP range for the cluster"
  default       = "10.0.0.0/13"
}

variable "pod_ip_cidr_range" {
  type        = string
  description = "Secondary IP range for pods"
  default     = "10.8.0.0/13"
}

variable "service_ip_cidr_range" {
  type        = string
  description = "Secondary IP range for services"
  default     = "10.16.0.0/13"
}

variable "ip_cidr_range_pub" {
  type          = string
  description   = "The IP range for the network public"
  default       = "10.24.0.0/13"
}

variable "ip_cidr_range_priv" {
  type        = string
  description = "The IP range for the network private"
  default     = "10.32.0.0/13"
}

# -------------------------------------------------------------
# Compute Instances Vars
#
variable "pub_ssh_user" {
  type        = string
  description = "public SSH default user"
  default     = "opc"
}

variable "pub_ssh_key_file" {
  type        = string
  description = "Path ssh file public key"
  default     = "ssh/pub_ssh_key_file.pem"
}

variable "priv_ssh_user" {
  type        = string
  description = "private SSH default user"
  default     = "opc"
}

variable "priv_ssh_key_file" {
  type        = string
  description = "Path ssh file public key"
  default     = "ssh/priv_ssh_key_file.pem"
}

# -----------------------------------------------------------
# GKE vars
#
variable "kubernetes_version" {
  type        = string
  description = "The Kubernetes version. Defaults to latest available"
  default     = ""
}

variable "gke_num_nodes" {
  default     = 1
  description = "number of gke nodes"
}

variable "cluster_name" {
  type        = string
  description = "The GKE cluster name"
}

variable "cluster_description" {
  type = string
  default = "This cluster is defined in GitLab"
  description = "A description for the cluster. We recommend adding the $CI_PROJECT_URL variable to describe where the cluster is configured."
}

variable "regional_cluster" {
  type        = string
  description = "Creates a cluster with multiple masters spread across zones in the region; forces new resource when changed"
  default     = "true"
}

variable "oauth_scopes" {
  type = list(string)

  default = [
    "https://www.googleapis.com/auth/compute",
    "https://www.googleapis.com/auth/devstorage.read_only",
    "https://www.googleapis.com/auth/logging.write",
    "https://www.googleapis.com/auth/monitoring",
  ]

  description = "The set of Google API scopes to be made available on all of the node VMs"
}

variable "enable_workload_identity" {
  type        = bool
  description = "Enable GKE Workload Identity support on cluster https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity"
  default     = false
}

# name - Provide a name for the node pool to be created, must be unique
# initial_node_count - set the starting size of the node_pool per zone, setting this to 0, creates an inoperable node_pool
# machine_type - The type of VM for the worker nodes
# max_node_count - The maximum number nodes per zone
# node_auto_repair - Whether the nodes will be automatically repaired
# node_auto_upgrade - Whether the nodes will be automatically upgraded
# node_disk_size - Size of the disk attached to each node, specified in GB. Forces new resource.
# node_disk_type - Type of the disk attached to each node. Forces new resource.
# preemptible - Use preemptible instances for this node pool
# kubernetes_version - The version of Kubernetes to use for this node pool. If not specified, it defaults to the value of kubernetes_version

variable "node_pools" {
  default = {}
}

# !!IMPORTANT!!
# These values are used as defaults when values are not specified for an array of
# node pools. Any change to these values should be a BREAKING CHANGE and should correspond
# to the most common used values for a production cluster

variable "default_node_pools" {
  default = {
    "default" = {
      name               = "node-pool-0"
      initial_node_count = 1
      machine_type       = "e2-standard-2"
      max_node_count     = 2
      node_auto_repair   = true
      node_auto_upgrade  = true
      node_disk_size_gb  = 100
      node_disk_type     = "pd-ssd"
      preemptible        = false
      taints             = ""
      kubernetes_version = ""
      enable_integrity_monitoring =  false
    },
  }

  description = "Array hosting various configurations for node pools"
}