# Reference: https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_subnetwork
#
resource "google_compute_subnetwork" "gke" {
  name                      = var.subnet_nat_name == null ? format("%v-gke", var.cluster_name) : var.subnet_nat_name
  network                   = google_compute_network.vpc.name
  project                   = var.project
  region                    = var.region
  ip_cidr_range             = var.ip_cidr_range
  private_ip_google_access  = "true"
  description               = "Exclusive network for Kubernetes cluster."

  secondary_ip_range {
    range_name    = "pod-range-1"
    ip_cidr_range = var.pod_ip_cidr_range
  }

  secondary_ip_range {
    range_name    = "service-range-1"
    ip_cidr_range = var.service_ip_cidr_range
  }

  log_config {
    aggregation_interval = "INTERVAL_10_MIN"
    flow_sampling        = 0.5
    metadata             = "INCLUDE_ALL_METADATA"
  }
}

resource "google_compute_subnetwork" "pub" {
  name                      = "${var.vpc}-pub"
  network                   = google_compute_network.vpc.name
  project                   = var.project
  region                    = var.region
  ip_cidr_range             = var.ip_cidr_range_pub
  private_ip_google_access  = "false"
  description               = "Network for instances and services that can be made public."
}

resource "google_compute_subnetwork" "priv" {
  name                      = "${var.vpc}-priv"
  network                   = google_compute_network.vpc.name
  project                   = var.project
  region                    = var.region
  ip_cidr_range             = var.ip_cidr_range_priv
  private_ip_google_access  = "true"
  description               = "Network for private instances and services, without direct public access."
}